package main

import (
	"flag"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"
	"os"
	"sbpstu-2023-bdas/sbpstu-2023-bdas-lab3/internal/app/client"
)

var pathToConfig = flag.String("config", "configs/config", "Path to YAML configuration")

func main() {
	l, _ := zap.NewProduction()

	flag.Parse()

	file, err := os.Open(*pathToConfig)
	if err != nil {
		l.Fatal("error while open YAML configuration file", zap.Error(err))
	}

	var appConfig *client.Config
	if err := yaml.NewDecoder(file).Decode(&appConfig); err != nil {
		l.Fatal("error while decode YAML configuration", zap.Error(err))
	}

	app, err := client.NewApp(appConfig, l)
	if err != nil {
		l.Fatal("error while create new client app", zap.Error(err))
	}

	if err := app.Invoke(); err != nil {
		l.Fatal("error while send request to https mtls server", zap.Error(err))
	}
}
