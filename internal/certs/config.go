package certs

type TLSConfiguration struct {
	Enabled  bool   `yaml:"enabled"`
	CertFile string `yaml:"cert-file"`
	KeyFile  string `yaml:"key-file"`
}

type CAConfiguration struct {
	Enabled bool   `yaml:"enabled"`
	CaFile  string `yaml:"ca-file"`
}
