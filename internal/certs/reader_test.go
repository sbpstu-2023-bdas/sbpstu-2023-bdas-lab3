package certs

import (
	"crypto/tls"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestReadKeyPair(t *testing.T) {
	tests := []struct {
		name    string
		paths   *TLSConfiguration
		isError bool
	}{
		{
			name: "good configuration",
			paths: &TLSConfiguration{
				Enabled:  true,
				CertFile: "testdata/test-crt.crt",
				KeyFile:  "testdata/test-key.key",
			},
			isError: false,
		},
		{
			name: "disabled option",
			paths: &TLSConfiguration{
				Enabled: false,
			},
			isError: true,
		},
		{
			name: "key not set",
			paths: &TLSConfiguration{
				Enabled:  true,
				CertFile: "testdata/test-crt.crt",
			},
			isError: true,
		},
		{
			name: "cert not set",
			paths: &TLSConfiguration{
				Enabled: true,
				KeyFile: "testdata/test-key.key",
			},
			isError: true,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			cert, err := NewReadKeyPair(test.paths)
			if test.isError {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				require.NotEqual(t, tls.Certificate{}, cert)
			}
		})
	}
}

func TestNewCertPool(t *testing.T) {
	path := &CAConfiguration{
		Enabled: false,
	}
	_, err := NewCertPool(path)
	require.Error(t, err)

	path = &CAConfiguration{
		Enabled: true,
		CaFile:  "testdata/test-ca.crt",
	}

	pool, err := NewCertPool(path)
	require.NoError(t, err)
	require.NotNil(t, pool)

}
