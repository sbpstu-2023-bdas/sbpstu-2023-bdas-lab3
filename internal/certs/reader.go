package certs

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io"
	"os"
)

func NewReadKeyPair(paths *TLSConfiguration) (tls.Certificate, error) {
	if !paths.Enabled {
		return tls.Certificate{}, errors.New("tls configuration disabled")
	}
	cert, err := tls.LoadX509KeyPair(paths.CertFile, paths.KeyFile)
	if err != nil {
		return tls.Certificate{}, fmt.Errorf("error while loading certs: %w", err)
	}
	return cert, nil
}

func NewCertPool(path *CAConfiguration) (*x509.CertPool, error) {
	if !path.Enabled {
		return nil, errors.New("ca configuration disabled")
	}
	caFile, err := os.Open(path.CaFile)
	if err != nil {
		return nil, fmt.Errorf("error while open CA file: %w", err)
	}
	cert, err := io.ReadAll(caFile)
	if err != nil {
		return nil, fmt.Errorf("error while reading CA cert: %w", err)
	}
	pool := x509.NewCertPool()
	pool.AppendCertsFromPEM(cert)
	return pool, nil
}
