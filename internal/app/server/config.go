package server

import (
	"sbpstu-2023-bdas/sbpstu-2023-bdas-lab3/internal/certs"
)

type Config struct {
	Host string                  `yaml:"host"`
	Tls  *certs.TLSConfiguration `yaml:"tls"`
	Ca   *certs.CAConfiguration  `yaml:"ca"`
}
