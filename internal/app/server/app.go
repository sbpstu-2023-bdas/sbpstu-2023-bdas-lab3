package server

import (
	"crypto/tls"
	"fmt"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
	"net/http"
	"sbpstu-2023-bdas/sbpstu-2023-bdas-lab3/internal/certs"
)

type App struct {
	server *http.Server
	l      *zap.Logger
}

func NewApp(config *Config, l *zap.Logger) (*App, error) {
	tlsConfig := &tls.Config{}

	if config.Tls.Enabled {
		cert, err := certs.NewReadKeyPair(config.Tls)
		if err != nil {
			return nil, fmt.Errorf("error while configure tls: %w", err)
		}
		tlsConfig.Certificates = []tls.Certificate{cert}
		tlsConfig.ClientAuth = tls.RequireAndVerifyClientCert
	}

	if config.Ca.Enabled {
		pool, err := certs.NewCertPool(config.Ca)
		if err != nil {
			return nil, fmt.Errorf("error while create new certs pool: %w", err)
		}
		tlsConfig.ClientCAs = pool
	}

	router := mux.NewRouter()
	router.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "pong")
		w.WriteHeader(http.StatusOK)
	}).Methods(http.MethodGet)

	return &App{
		server: &http.Server{
			Addr:      config.Host,
			Handler:   router,
			TLSConfig: tlsConfig,
		},
		l: l,
	}, nil
}

func (a *App) Serve() error {
	a.l.Info("start serve HTTP server",
		zap.String("host", a.server.Addr),
		zap.Bool("tls", len(a.server.TLSConfig.Certificates) > 0),
		zap.Bool("mtls", a.server.TLSConfig.RootCAs != nil))
	return a.server.ListenAndServeTLS("", "")
}

func (a *App) Close() error {
	a.l.Info("Close HTTP server connection",
		zap.String("host", a.server.Addr))
	return a.server.Close()
}
