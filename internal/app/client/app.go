package client

import (
	"crypto/tls"
	"fmt"
	"go.uber.org/zap"
	"io"
	"net/http"
	"sbpstu-2023-bdas/sbpstu-2023-bdas-lab3/internal/certs"
)

type App struct {
	client         *http.Client
	serverEndpoint string
	l              *zap.Logger
}

func NewApp(config *Config, l *zap.Logger) (*App, error) {
	tlsConfig := &tls.Config{}

	if config.Tls.Enabled {
		cert, err := certs.NewReadKeyPair(config.Tls)
		if err != nil {
			return nil, fmt.Errorf("error while configure tls: %w", err)
		}
		tlsConfig.Certificates = []tls.Certificate{cert}
		tlsConfig.ClientAuth = tls.RequireAndVerifyClientCert
	}

	if config.Ca.Enabled {
		pool, err := certs.NewCertPool(config.Ca)
		if err != nil {
			return nil, fmt.Errorf("error while create new certs pool: %w", err)
		}
		tlsConfig.RootCAs = pool
	}

	return &App{
		client: &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: tlsConfig,
			},
		},
		serverEndpoint: config.ServerEndpoint,
		l:              l,
	}, nil
}

func (a *App) Invoke() error {
	resp, err := a.client.Get(a.serverEndpoint)
	if err != nil {
		return fmt.Errorf("error while request to https mtls server: %w", err)
	}
	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("error while read resp body data: %w", err)
	}

	a.l.Info("get data from https mtls server",
		zap.String("endpoint", a.serverEndpoint),
		zap.Int("code", resp.StatusCode),
		zap.String("response", string(data)))
	return nil
}
