package client

import (
	"sbpstu-2023-bdas/sbpstu-2023-bdas-lab3/internal/certs"
)

type Config struct {
	ServerEndpoint string                  `yaml:"server-endpoint"`
	Tls            *certs.TLSConfiguration `yaml:"tls"`
	Ca             *certs.CAConfiguration  `yaml:"ca"`
}
